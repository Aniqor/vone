import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
private  recipes: Recipe[] = [
    {
      id: '1',
      title: 'Dambadarjaa',
      imageUrl:'assets/imgs/Damba.png',
      facebook: 'Damba8Darjaa',
 

     
    },
    {
      id: '2',
      title: 'Kamikaze',
      imageUrl:'assets/imgs/Kamikaze.png',
      facebook: 'Damba8Darjaa',
      css:'assets/imgs/css.png',
      ingredients: ['French frsdies', 'pork meadt', 'sdsalaad']
     
    } 
  ]; 
  constructor() { }

  getAllRecipes(){
    return [...this.recipes]
  }
  getRecipe(recipeId: string) {
    return {
      ...this.recipes.find(recipe => {
      return recipe.id === recipeId;
    })};

  }
}
